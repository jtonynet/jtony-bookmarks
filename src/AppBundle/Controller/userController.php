<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class userController extends Controller
{
    private $apiRoot = 'https://jtony-bookmarks-api.herokuapp.com'; //PROD
    #private $apiRoot = 'http://jtony-bookmarks-api.com'; //LOCAL

    private $domain = 'https://jtony-bookmarks.herokuapp.com'; //PROD
    #private $domain = 'http://jtony-bookmarks.com'; //LOCAL

    /**
     * @Route("/user/auth")
     */
    public function userAuthAction(Request $request)
    {
        $apiEndpoint = $this->apiRoot."/users/auth";

        $session = $request->getSession();
        $session->set('user_message', null);

        $message = 'Error, try again later';
        $token = null;

        try{
            $password = $request->get("password");
            $email = $request->get("email");

            $response = \Httpful\Request::post($apiEndpoint)
                ->sendsType(\Httpful\Mime::FORM)
                ->body('password='.$password.'&email='.$email)
                ->send();

            $httpCode = $response->code;
            $responseBody = $response->body;
            $responseBody = json_decode($responseBody);

            $message = $responseBody->message;
            
            if($httpCode==Response::HTTP_UNAUTHORIZED) {
                    $session->set('user', null);
                    $session->set('user_message', $message);
                    return $this->redirect($this->domain.'/user');
            } else if($httpCode==Response::HTTP_OK) {
                $token = !is_null($response->headers['x-access-token']) ? $response->headers['x-access-token'] : null;
                $user = $responseBody->resource;
                $user->token = $token;

                if($token) {
                    $session->set('user', json_encode($user));
                    return $this->redirect($this->domain.'/user/bookmarks');
                }
            } else if(is_object($message)) {                
                $strMessage = $this->getErrorsFromObj($message);
                $message = $strMessage;
            }

            $session->set('user_message', $message);
            
        } catch(Exception $e) {
            $session->set('user_message', $message);
            return $this->redirectToRoute('/user');
        }

        return $this->redirect($this->domain.'/user');
    }    

    /**
     * @Route("/user/bookmark/create")
     */
    public function userBookmarkCreate(Request $request)
    {
        $session = $request->getSession();

        $userMessage = $session->get('user_message');
        $session->set('user_message', null);

        $message = 'Error, try again later';
        $user = $session->get('user');

        if(!$user) {
            $session->set('user_message', 'Authorization error, try again');
            return $this->redirect($this->domain.'/user');
        } else {
            $user = json_decode($user);
        }

        return $this->render('user/form_add_bookmarks.html.twig', array(
            'save_bookmark_action' => $this->domain.'/user/bookmark/save',
            'bookmark_list_url' =>  $this->domain.'/user/bookmarks',
            'logout_url' =>  $this->domain.'/user/logout',
            'user_nick' => $user->nick,
            'bookmark' => null,
            'user_message' => $userMessage,
        ));
    }

    /**
     * @Route("/user/bookmark/save")
     */
    public function userBookmarkSave(Request $request)
    {
        $session = $request->getSession();
        $session->set('user_message', null);

        $message = 'Error, try again later';
        $user = $session->get('user');

        if($user){
            try{
                $url = $request->get("bookmark");
            
                $user = json_decode($user);
                $apiEndpoint = $this->apiRoot.'/users/'.$user->id.'/bookmarks';

                $response = \Httpful\Request::post($apiEndpoint)
                    ->sendsType(\Httpful\Mime::FORM)
                    ->addHeader('x-access-token', $user->token)
                    ->body('url='.$url)
                    ->send();

                $httpCode = $response->code;
                $responseBody = $response->body;
                $responseBody = json_decode($responseBody);

                $message = $responseBody->message;
                
                if($httpCode==Response::HTTP_UNAUTHORIZED) {
                    $session->set('user', null);
                    $session->set('user_message', $message);
                    return $this->redirect($this->domain.'/user');
                } else if($httpCode==Response::HTTP_OK) {
                    return $this->redirect($this->domain.'/user/bookmarks');
                } else if(is_object($message)) {
                    $strMessage = $this->getErrorsFromObj($message);
                } else {
                    $strMessage = $message;
                }

                $session->set('user_message', $strMessage);
            } catch(Exception $e) {
                $session->set('user_message', $message);
                return $this->redirect($this->domain.'/user');
            }
        }

        return $this->redirect($this->domain.'/user/bookmark/create');
    }

    /**
     * @Route("/user/bookmark/edit/{id}")
     */
    public function userBookmarkEdit($id, Request $request)
    {
        $id = (int)$id;
        $session = $request->getSession();

        $userMessage = $session->get('user_message');
        $session->set('user_message', null);        

        $message = 'Error, try again later';
        $user = $session->get('user');

        if($user){
            try{
                $user = json_decode($user);
                $apiEndpoint = $this->apiRoot.'/bookmarks/'.$id;

                $response = \Httpful\Request::get($apiEndpoint)
                    ->addHeader('x-access-token', $user->token)
                    ->send();

                $httpCode = $response->code;
                $responseBody = $response->body;
                $responseBody = json_decode($responseBody);

                $message = $responseBody->message;
                
                if($httpCode==Response::HTTP_UNAUTHORIZED) {
                    $session->set('user', null);
                    $session->set('user_message', $message);
                    return $this->redirect($this->domain.'/user');
                } else if($httpCode==Response::HTTP_OK) {


                    return $this->render('user/form_add_bookmarks.html.twig', array(
                        'save_bookmark_action' => $this->domain.'/user/bookmark/update',
                        'bookmark_list_url' =>  $this->domain.'/user/bookmarks',
                        'logout_url' =>  $this->domain.'/user/logout',
                        'user_nick' => $user->nick,
                        'bookmark' => get_object_vars($responseBody->resource),
                        'user_message' => $userMessage,
                    ));
                } 

                $session->set('user_message', $message);

            } catch(Exception $e) {
                $session->set('user_message', $message);
                return $this->redirect($this->domain.'/user/bookmarks');
            }
        }

        return $this->redirect($this->domain.'/user/bookmarks');
    }

    /**
     * @Route("/user/bookmark/update")
     */
    public function userBookmarkUpdate(Request $request)
    {
        $id = (int)$id;
        $session = $request->getSession();

        $message = 'Error, try again later';
        $user = $session->get('user');

        if($user){
            try{
                $url = $request->get("bookmark");
                $id = $request->get("id");
            
                $user = json_decode($user);
                $apiEndpoint = $this->apiRoot.'/bookmarks/'.$id;

                $response = \Httpful\Request::post($apiEndpoint)
                    ->sendsType(\Httpful\Mime::FORM)
                    ->body('id='.$id.'&url='.$url)
                    ->addHeader('x-access-token', $user->token)
                    ->send();

                $httpCode = $response->code;
                $responseBody = $response->body;
                $responseBody = json_decode($responseBody);

                $message = $responseBody->message;
                
                if($httpCode==Response::HTTP_UNAUTHORIZED) {
                    $session->set('user', null);
                    $session->set('user_message', $message);
                    return $this->redirect($this->domain.'/user');
                } else if($httpCode==Response::HTTP_OK) {
                    return $this->redirect($this->domain.'/user/bookmarks');
                } else if(is_object($message)) {
                    $strMessage = $this->getErrorsFromObj($message);
                } else {
                    $strMessage = $message;
                }


                $session->set('user_message', $strMessage);
                
            } catch(Exception $e) {
                $session->set('user_message', $message);
                return $this->redirect($this->domain.'/user/bookmark/edit/'.$id);
            }
        }

        return $this->redirect($this->domain.'/user/bookmark/edit/'.$id);
    }


    /**
     * @Route("/user/bookmark/delete/{id}")
     */
    public function userBookmarkDelete($id, Request $request)
    {
        $id = (int)$id;
        $session = $request->getSession();

        $message = 'Error, try again later';
        $user = $session->get('user');

        if($user){
            try{
                $url = $request->get("bookmark");
            
                $user = json_decode($user);
                $apiEndpoint = $this->apiRoot.'/bookmarks/'.$id;

                $response = \Httpful\Request::delete($apiEndpoint)
                    ->sendsType(\Httpful\Mime::FORM)
                    ->addHeader('x-access-token', $user->token)
                    ->send();

                $httpCode = $response->code;
                $responseBody = $response->body;
                $responseBody = json_decode($responseBody);

                $message = $responseBody->message;
                
                if($httpCode==Response::HTTP_UNAUTHORIZED) {
                    $session->set('user', null);
                    $session->set('user_message', $message);
                    return $this->redirect($this->domain.'/user');
                } else if($httpCode==Response::HTTP_OK) {
                    return $this->redirect($this->domain.'/user/bookmarks');
                } else if(is_object($message)) {
                    $strMessage = $this->getErrorsFromObj($message);
                } else {
                    $strMessage = $message;
                }

                $session->set('user_message', $strMessage);
            } catch(Exception $e) {
                $session->set('user_message', $message);
                return $this->redirect($this->domain.'/user/bookmarks');
            }
        }

        return $this->redirect($this->domain.'/user/bookmarks');
    }

    /**
     * @Route("/user/bookmarks")
     */
    public function userBookmarks(Request $request)
    {
        $session = $request->getSession();

        $userMessage = $session->get('user_message');
        $session->set('user_message', null);

        $message = 'Error, try again later';
        $user = $session->get('user');

        if($user){
            $user = json_decode($user);

            $apiEndpoint = $this->apiRoot."/users/".$user->id."/bookmarks";

            try{
                $response = \Httpful\Request::get($apiEndpoint)
                    ->addHeader('x-access-token', $user->token)
                    ->send();

                $httpCode = $response->code;

                $responseBody = $response->body;
                $responseBody = json_decode($responseBody);

                $message = $responseBody->message;
                $resource = $responseBody->resource;

                $bookmarks = [];

                if($httpCode==Response::HTTP_UNAUTHORIZED) {
                    $session->set('user', null);
                    $session->set('user_message', $message);
                    return $this->redirect($this->domain.'/user');
                } else if ($httpCode!=Response::HTTP_OK){
                    $session->set('user_message', $message); 
                    return $this->redirect($this->domain.'/user');                   
                }else if(is_array($resource->bookmarkList)) {
                    foreach ($resource->bookmarkList as $key => $value) {
                        $bookmarks[]=array('id'=>$value->id, 'url'=>$value->url);
                    }
                }

            } catch(Exception $e) {
                $session->set('user_message', $message);
                return $this->redirect($this->domain.'/user');
            }

            return $this->render('user/table_bookmarks_user.html.twig', array(
                'user_nick' => $user->nick,
                'bookmarks' => $bookmarks,
                'bookmark_create_url' => $this->domain.'/user/bookmark/create',
                'logout_url' =>  $this->domain.'/user/logout',
                'bookmark_delete_url' =>  $this->domain.'/user/bookmark/delete',
                'bookmark_edit_url' =>  $this->domain.'/user/bookmark/edit',
                'user_message' => $userMessage,
            ));
        }

        $session->set('user_message', $message);
        return $this->redirect($this->domain.'/user');
    }

    /**
     * @Route("/user/create")
     */
    public function userCreate(Request $request)
    {
        $session = $request->getSession();

        $userMessage = $session->get('user_message');
        $session->set('user_message', null);

        $message = 'Error, try again later';
        $user = $session->get('user');

        if($user) {
            $session->set('user_message', 'make logout before create a new user');
            return $this->redirect($this->domain.'/user/bookmarks');
        }

        return $this->render('user/form_add_user.html.twig', array(
            'user_message' => $userMessage,
            'save_user_action' => $this->domain.'/user/save',
            'logout_url' =>  $this->domain.'/user/logout'
        ));
    }

    /**
     * @Route("/user/save")
     */
    public function userSave(Request $request)
    {
        $session = $request->getSession();
        $session->set('user_message', null);

        $message = 'Error, try again later';

        try{
            $name = $request->get("name");
            $nick = $request->get("nick");
            $email = $request->get("email");
            $password = $request->get("password");
        
            $user = json_decode($user);
            $apiEndpoint = $this->apiRoot.'/users';

            $response = \Httpful\Request::post($apiEndpoint)
                ->sendsType(\Httpful\Mime::FORM)
                ->body(
                    'name='.$name.'&'.
                    'nick='.$nick.'&'.
                    'email='.$email.'&'.
                    'password='.$password)
                ->send();

            $httpCode = $response->code;
            $responseBody = $response->body;
            $responseBody = json_decode($responseBody);

            $message = $responseBody->message;

            if($httpCode==Response::HTTP_UNAUTHORIZED) {
                    return $this->redirect($this->domain.'/user');
            } else if($httpCode==Response::HTTP_OK) {
                $token = !is_null($response->headers['x-access-token']) ? $response->headers['x-access-token'] : null;
                $user = $responseBody->resource;
                $user->token = $token;

                if($token) {
                    $session->set('user', json_encode($user));
                    return $this->redirect($this->domain.'/user/bookmarks');
                }
            } else {                
                if(is_object($message))
                    $message = $this->getErrorsFromObj($message);

                $session->set('user_message', $message);
                return $this->redirect($this->domain.'/user/create');
            }

            $session->set('user_message', $message);
        } catch(Exception $e) {
            $session->set('user_message', $message);
            return $this->redirect($this->domain.'/user/create');
        }
    }    

    /**
     * @Route("/user")
     */
    public function userAction(Request $request)
    {
        $session = $request->getSession();
        $userMessage = $session->get('user_message');
        $session->set('user_message', null);

        return $this->render('user/form_login_user.html.twig', array(
            'user_auth_action' => $this->domain.'/user/auth',
            'user_message' => $userMessage,
            'user_create_url' => $this->domain.'/user/create'
        ));
    }

    /**
     * @Route("/user/logout")
     */
    public function userLogout(Request $request)
    {
        $session = $request->getSession();
        $session->set('user_message', 'User logout');
        $session->set('user', null);

        return $this->userAction($request);
    }

    private function getErrorsFromObj($objErrors){
        $objErrors = get_object_vars($objErrors);
        $strMessage = '';
        foreach ($objErrors as $key => $value) {
            $strMessage .= 'Field '.$key.' '.$value.' - ';
        }

        return $strMessage;
    }    


}