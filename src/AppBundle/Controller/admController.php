<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class admController extends Controller
{
    private $apiRoot = 'https://jtony-bookmarks-api.herokuapp.com'; //PROD
    #private $apiRoot = 'http://jtony-bookmarks-api.com'; //LOCAL

    private $domain = 'https://jtony-bookmarks.herokuapp.com'; //PROD
    #private $domain = 'http://jtony-bookmarks.com'; //LOCAL

    /**
     * @Route("/adm/auth")
     */
    public function admAuthAction(Request $request)
    {
        $apiEndpoint = $this->apiRoot."/admins/auth";

        $session = $request->getSession();
        $session->set('adm_message', null);

        $message = 'Error, try again later';
        $token = null;

        try{
            $password = $request->get("password");
            $email = $request->get("email");

            $response = \Httpful\Request::post($apiEndpoint)
                ->sendsType(\Httpful\Mime::FORM)
                ->body('password='.$password.'&email='.$email)
                ->send();

            $httpCode = $response->code;
            $responseBody = $response->body;
            $responseBody = json_decode($responseBody);

            $message = $responseBody->message;
            
            if($httpCode==Response::HTTP_UNAUTHORIZED) {
                    $session->set('adm', null);
                    $session->set('adm_message', $message);
                    return $this->redirect($this->domain.'/adm');
            } else if($httpCode==Response::HTTP_OK) {
                $token = !is_null($response->headers['x-access-token']) ? $response->headers['x-access-token'] : null;
                $adm = $responseBody->resource;
                $adm->token = $token;

                if($token) {
                    $session->set('adm', json_encode($adm));
                    return $this->redirect($this->domain.'/adm/users');
                }
            }

            $session->set('adm_message', $message);
            
        } catch(Exception $e) {
            $session->set('adm_message', $message);
            return $this->redirectToRoute('/adm');
        }

        return $this->redirect($this->domain.'/adm');
    }    

    /**
     * @Route("/adm")
     */
    public function admAction(Request $request)
    {
        $session = $request->getSession();
        $admMessage = $session->get('adm_message');
        $session->set('adm_message', null);

        return $this->render('adm/form_login_adm.html.twig', array(
            'adm_auth_action' => $this->domain.'/adm/auth',
            'adm_message' => $admMessage
        ));
    }

    /**
     * @Route("/adm/users")
     */
    public function admUsers(Request $request)
    {
        $session = $request->getSession();
        $session->set('user_message', null);

        $message = 'Error, try again later';
        $adm = $session->get('adm');

        if($adm){
            try {
                $adm = json_decode($adm);
                $apiEndpoint = $this->apiRoot.'/admins/'.$adm->id.'/users';

                $response = \Httpful\Request::get($apiEndpoint)
                    ->addHeader('x-access-token', $adm->token)
                    ->send();

                $httpCode = $response->code;
                $responseBody = $response->body;
                $responseBody = json_decode($responseBody);

                $message = $responseBody->message;
                $resource = $responseBody->resource;

                if($httpCode==Response::HTTP_UNAUTHORIZED) {
                    $session->set('adm', null);
                    $session->set('adm_message', $message);
                    return $this->redirect($this->domain.'/adm');
                } else if ($httpCode==Response::HTTP_OK){
                    return $this->render('adm/table_users.html.twig', array(
                        'adm_nick' => $adm->nick,
                        'adm_message' => '',
                        'logout_url' =>  $this->domain.'/adm/logout',
                        'users' =>  $resource->userList,
                    ));
                } else {
                    $session->set('adm_message', $message); 
                    return $this->redirect($this->domain.'/adm');                   
                    echo "VTNC";exit;
                }
            } catch(Exception $e) {
                $session->set('adm_message', $message);
                return $this->redirect($this->domain.'/adm');
            }
        }

        $session->set('adm_message', $message);
        return $this->redirect($this->domain.'/adm');
    }


    /**
     * @Route("adm/user/{idUser}/bookmarks")
     */
    public function admUserBookmarks($idUser, Request $request)
    {
        $idUser = (int)$idUser;
        $session = $request->getSession();
        $session->set('adm_message', null);

        $message = 'Error, try again later';
        $adm = $session->get('adm');


        if($adm){
            $adm = json_decode($adm);

            $apiEndpoint = $this->apiRoot."/users/".$idUser."/bookmarks";

            try{
                $response = \Httpful\Request::get($apiEndpoint)
                    ->addHeader('x-access-token', $adm->token)
                    ->send();

                $httpCode = $response->code;

                $responseBody = $response->body;
                $responseBody = json_decode($responseBody);

                $message = $responseBody->message;
                $resource = $responseBody->resource;

               # var_dump();exit;

                $bookmarks = [];

                if($httpCode==Response::HTTP_UNAUTHORIZED) {
                    $session->set('adm', null);
                    $session->set('adm_message', $message);
                    return $this->redirect($this->domain.'/user');
                } else if ($httpCode!=Response::HTTP_OK){
                    $session->set('adm_message', $message); 
                    return $this->redirect($this->domain.'/adm');                   
                } else {
                    $bookmarks = $responseBody->resource->bookmarkList;
                }

            } catch(Exception $e) {
                $session->set('adm_message', $message);
                return $this->redirect($this->domain.'/adm');
            }

            return $this->render('adm/table_user_bookmarks.html.twig', array(
                'adm_nick' => $adm->nick,
                'bookmarks' => $bookmarks,
                'logout_url' =>  $this->domain.'/adm/logout',
                'user_list_url' => $this->domain.'/adm/users'
            ));
        }

        $session->set('user_message', $message);
        return $this->redirect($this->domain.'/user');
    }    

    /**
     * @Route("/adm/logout")
     */
    public function admLogout(Request $request)
    {
        $session = $request->getSession();
        $session->set('adm_message', 'Adm logout');
        $session->set('adm', null);

        return $this->admAction($request);
    }

}